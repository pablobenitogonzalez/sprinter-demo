package com.example.demo.services;

import com.example.demo.ProductTestHelper;
import com.example.demo.dtos.ProductDto;
import com.example.demo.entities.Product;
import com.example.demo.mappers.ProductMapper;
import com.example.demo.repositories.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * The type Product service test.
 */
@RunWith(SpringRunner.class)
public class ProductServiceTest {

    @Mock
    private ProductMapper productMapper;

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductServiceImpl productService;

    /**
     * Sets up.
     */
    @Before
    public void setUp() {
    }

    /**
     * When find all then all products should be found.
     */
    @Test
    public void whenFindAll_thenAllProductsShouldBeFound() {
        // given
        final Long productId = 1L;
        final Product product = ProductTestHelper.buildProduct(productId);
        final PageImpl<Product> page = new PageImpl<>(Collections.singletonList(product));
        Mockito.when(productRepository.findAll(Mockito.any(Pageable.class))).thenReturn(page);
        Mockito.when(productMapper.toDtos(Mockito.anyList()))
                .thenReturn(Collections.singletonList(ProductTestHelper.buildProductDto(productId)));

        // when
        final Page<ProductDto> result = productService.findAll(Pageable.unpaged());

        // then
        assertThat(result).isNotNull();
        assertThat(result.getContent().size()).isEqualTo(1);
    }

    /**
     * When find then product should be found.
     */
    @Test
    public void whenFind_thenProductShouldBeFound() {
        // given
        final Long productId = 1L;
        final Product product = ProductTestHelper.buildProduct(productId);
        Mockito.when(productRepository.findById(product.getId())).thenReturn(Optional.of(product));
        Mockito.when(productMapper.toDto(product)).thenReturn(ProductTestHelper.buildProductDto(productId));

        // when
        final ProductDto result = productService.find(productId);

        // then
        assertThat(result).isNotNull();
        assertThat(result.getName()).isEqualTo(product.getName());
        assertThat(result.getDescription()).isEqualTo(product.getDescription());
        assertThat(result.getPrice()).isEqualTo(product.getPrice());
    }

    /**
     * When create then product should be created.
     */
    @Test
    public void whenCreate_thenProductShouldBeCreated() {
        // given
        final Product product = ProductTestHelper.buildProduct();
        final ProductDto productDto = ProductTestHelper.buildProductDto();
        Mockito.when(productRepository.save(Mockito.any(Product.class))).thenReturn(product);
        Mockito.when(productMapper.toDto(product)).thenReturn(productDto);
        Mockito.when(productMapper.toEntity(productDto)).thenReturn(product);

        // when
        final ProductDto result = productService.create(productDto);

        // then
        assertThat(result).isNotNull();
    }

    /**
     * When update then product should be updated.
     */
    @Test
    public void whenUpdate_thenProductShouldBeUpdated() {
        // given
        final Long productId = 1L;
        final Product product = ProductTestHelper.buildProduct(productId);
        final ProductDto productDto = ProductTestHelper.buildProductDto(productId);
        Mockito.when(productRepository.findById(product.getId())).thenReturn(Optional.of(product));
        Mockito.when(productRepository.save(Mockito.any(Product.class))).thenReturn(product);
        Mockito.when(productMapper.toDto(Mockito.any(Product.class))).thenReturn(productDto);
        Mockito.when(productMapper.toEntity(Mockito.any(ProductDto.class))).thenReturn(product);

        // when
        final ProductDto result = productService.update(productId, productDto);

        // then
        assertThat(result).isNotNull();
        assertThat(result.getId()).isEqualTo(productId);
    }

    /**
     * When delete then product should be deleted.
     */
    @Test
    public void whenDelete_thenProductShouldBeDeleted() {
        // given
        final Product product = ProductTestHelper.buildProduct(1L);
        Mockito.when(productRepository.findById(product.getId())).thenReturn(Optional.of(product));
        Mockito.doNothing().when(productRepository).delete(Mockito.any(Product.class));

        // when
        productService.delete(product.getId());
    }


}

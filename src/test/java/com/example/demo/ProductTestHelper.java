package com.example.demo;

import com.example.demo.dtos.ProductDto;
import com.example.demo.entities.Product;

import java.math.BigDecimal;

/**
 * The type Product test helper.
 */
public class ProductTestHelper {

    /**
     * Build product product.
     *
     * @return the product
     */
    public static Product buildProduct() {
        return ProductTestHelper.buildProduct(null);
    }

    /**
     * Build product product.
     *
     * @param id the id
     * @return the product
     */
    public static Product buildProduct(Long id) {
        final Product product = new Product();
        product.setId(id);
        product.setName("name");
        product.setDescription("description");
        product.setPrice(new BigDecimal(10));
        return product;
    }

    /**
     * Build product dto product dto.
     *
     * @return the product dto
     */
    public static ProductDto buildProductDto() {
        return ProductTestHelper.buildProductDto(null);
    }

    /**
     * Build product dto product dto.
     *
     * @param id the id
     * @return the product dto
     */
    public static ProductDto buildProductDto(Long id) {
        final ProductDto productDto = new ProductDto();
        productDto.setId(id);
        productDto.setName("name");
        productDto.setDescription("description");
        productDto.setPrice(new BigDecimal(10));
        return productDto;
    }

}

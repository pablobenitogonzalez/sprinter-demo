package com.example.demo.controllers;

import com.example.demo.ProductTestHelper;
import com.example.demo.dtos.ProductDto;
import com.example.demo.entities.Product;
import com.example.demo.repositories.ProductRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * The type Product controller integration test.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(properties={"net.sf.ehcache.disabled=true"})
public class ProductControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProductRepository repository;

    /**
     * When find all then products should be got.
     *
     * @throws Exception the exception
     */
    @Test
    public void whenFindAll_thenProductsShouldBeGot() throws Exception {
        repository.save(ProductTestHelper.buildProduct());
        mockMvc.perform(MockMvcRequestBuilders.get("/product")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0]").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content[0].id").isNotEmpty());
    }

    /**
     * When find then product should be got.
     *
     * @throws Exception the exception
     */
    @Test
    public void whenFind_thenProductShouldBeGot() throws Exception {
        final Product productDBO = repository.save(ProductTestHelper.buildProduct());
        mockMvc.perform(MockMvcRequestBuilders.get("/product/{id}", productDBO.getId())
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(productDBO.getId()));
    }

    /**
     * When create then product should be created.
     *
     * @throws Exception the exception
     */
    @Test
    public void whenCreate_thenProductShouldBeCreated() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/product")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(ProductTestHelper.buildProductDto())))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }

    /**
     * When update then product should be updated.
     *
     * @throws Exception the exception
     */
    @Test
    public void whenUpdate_thenProductShouldBeUpdated() throws Exception {
        final Product productDBO = repository.save(ProductTestHelper.buildProduct());
        final ProductDto changedProductDto = ProductTestHelper.buildProductDto(productDBO.getId());
        changedProductDto.setName("changed name");
        changedProductDto.setDescription("changed description");
        mockMvc.perform(MockMvcRequestBuilders.put("/product/{id}", productDBO.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(changedProductDto)))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("changed name"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value("changed description"));
    }

    /**
     * When delete then product should be deleted.
     *
     * @throws Exception the exception
     */
    @Test
    public void whenDelete_thenProductShouldBeDeleted() throws Exception {
        final Product productDBO = repository.save(ProductTestHelper.buildProduct());
        mockMvc.perform(MockMvcRequestBuilders.delete("/product/{id}", productDBO.getId()))
                .andExpect(status().isOk());
    }

}

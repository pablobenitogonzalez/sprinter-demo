package com.example.demo.repositories;

import com.example.demo.ProductTestHelper;
import com.example.demo.entities.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * The type Product repository integration test.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryIntegrationTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProductRepository productRepository;

    /**
     * When find by id then return product.
     */
    @Test
    public void whenFindById_thenReturnProduct() {
        // given
        final Product product = ProductTestHelper.buildProduct();
        final Product productDBO = entityManager.persist(product);
        entityManager.flush();

        // when
        final Product result = productRepository.findById(productDBO.getId()).orElse(null);

        // then
        assertThat(result).isNotNull();
        assertThat(result.getName()).isEqualTo(productDBO.getName());
        assertThat(result.getDescription()).isEqualTo(productDBO.getDescription());
        assertThat(result.getPrice()).isEqualTo(productDBO.getPrice());
    }

    /**
     * When save then return product.
     */
    @Test
    public void whenSave_thenReturnProduct() {
        // given
        final Product product = ProductTestHelper.buildProduct();

        // when
        final Product result = productRepository.save(product);

        // then
        assertThat(result).isNotNull();
        assertThat(result.getId()).isNotNull();
    }
}

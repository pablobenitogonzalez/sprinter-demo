package com.example.demo.dtos;

import com.example.demo.entities.Audit;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The type Order base dto.
 */
@Getter
@Setter
public class OrderBaseDto implements Serializable {
    private Long id;
    @NotEmpty
    private String applicant;
    private BigDecimal total;
    private Audit audit;
}

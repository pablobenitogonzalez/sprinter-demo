package com.example.demo.dtos;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The type Order line base dto.
 */
@Getter
@Setter
public class OrderLineBaseDto implements Serializable {
    private Long id;
    @NotNull
    private ProductDto product;
    @NotNull
    @Min(1)
    private Integer units;
    private BigDecimal total;
    private AuditDto audit;
}

package com.example.demo.dtos;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * The type Audit dto.
 */
@Getter
@Setter
public class AuditDto implements Serializable {
    private LocalDateTime createdOn;
    private LocalDateTime updatedOn;
}

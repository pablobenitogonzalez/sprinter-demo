package com.example.demo.dtos;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * The type Order line dto.
 */
@Getter
@Setter
public class OrderLineDto extends OrderLineBaseDto implements Serializable {
    private OrderBaseDto order;
}

package com.example.demo.dtos;

import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.List;

/**
 * The type Order dto.
 */
@Getter
@Setter
public class OrderDto extends OrderBaseDto implements Serializable {
    @Valid
    private List<OrderLineBaseDto> orderLines;
}

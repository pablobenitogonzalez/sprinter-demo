package com.example.demo.exceptions;

/**
 * The type Demo not found exception.
 */
public class DemoNotFoundException extends RuntimeException {
    /**
     * Instantiates a new Demo not found exception.
     *
     * @param message the message
     */
    public DemoNotFoundException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Demo not found exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public DemoNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}

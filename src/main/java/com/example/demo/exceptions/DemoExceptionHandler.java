package com.example.demo.exceptions;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Validation exception handler.
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class DemoExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * Handle resource not found exception response entity.
     *
     * @param e the e
     * @return the response entity
     */
    @ExceptionHandler(DemoNotFoundException.class)
    public ResponseEntity<Object> handleResourceNotFoundException(DemoNotFoundException e) {
        final ApiError err = ApiError.builder().timestamp(LocalDateTime.now()).status(HttpStatus.BAD_REQUEST)
                .message("Resource Not Found").errors(Collections.singletonList(e.getMessage())).build();
        return ResponseEntityBuilder.build(err);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
            HttpMessageNotReadableException e, HttpHeaders headers, HttpStatus status, WebRequest request) {
        final ApiError err = ApiError.builder().timestamp(LocalDateTime.now()).status(HttpStatus.BAD_REQUEST)
                .message("Malformed JSON request").errors(Collections.singletonList(e.getMessage())).build();
        return ResponseEntityBuilder.build(err);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException e, HttpHeaders headers, HttpStatus status, WebRequest request) {
        final List<String> details = e.getBindingResult().getFieldErrors().stream()
                .map(error -> String.format("%s : %s", error.getObjectName(), error.getDefaultMessage()))
                .collect(Collectors.toList());

        final ApiError err = ApiError.builder().timestamp(LocalDateTime.now()).status(HttpStatus.BAD_REQUEST)
                .message("Validation Errors").errors(details).build();
        return ResponseEntityBuilder.build(err);
    }
}

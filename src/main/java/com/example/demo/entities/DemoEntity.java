package com.example.demo.entities;

/**
 * The interface Demo entity.
 */
public interface DemoEntity {
    /**
     * Gets id.
     *
     * @return the id
     */
    Long getId();
}

package com.example.demo.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

/**
 * The type Audit.
 */
@Getter
@Setter
@Embeddable
public class Audit {

    @Column(name = "created_on")
    private LocalDateTime createdOn;

    @Column(name = "updated_on")
    private LocalDateTime updatedOn;

    /**
     * Pre persist.
     */
    @PrePersist
    public void prePersist() {
        this.createdOn = LocalDateTime.now();
        this.updatedOn = this.createdOn;
    }

    /**
     * Pre update.
     */
    @PreUpdate
    public void preUpdate() {
        this.updatedOn = LocalDateTime.now();
    }

}

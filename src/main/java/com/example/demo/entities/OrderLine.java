package com.example.demo.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The type Order line.
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
@Entity
@Table(name = "order_line")
public class OrderLine implements DemoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Order order;

    @ManyToOne(fetch = FetchType.LAZY)
    private Product product;

    private Integer units;

    private BigDecimal total;

    @Embedded
    private Audit audit;
}

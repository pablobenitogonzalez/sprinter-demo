package com.example.demo.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * The type Order.
 */
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
@Entity
@Table(name = "order_request")
public class Order implements DemoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false)
    private Long id;

    @Column(name = "applicant")
    private String applicant;

    @Column(name = "name")
    private BigDecimal total;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "order_id")
    private List<OrderLine> orderLines;

    @Embedded
    private Audit audit;
}

package com.example.demo.mappers;

import com.example.demo.dtos.ProductDto;
import com.example.demo.entities.Audit;
import com.example.demo.entities.Product;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

/**
 * The interface Product mapper.
 */
@Mapper(componentModel = "spring", uses = {AuditMapper.class})
public abstract class ProductMapper implements DemoMapper<Product, ProductDto> {
    /**
     * To dto product dto.
     *
     * @param entity the entity
     * @return the product dto
     */
    public abstract ProductDto toDto(Product entity);

    /**
     * To dtos list.
     *
     * @param entities the entities
     * @return the list
     */
    public abstract List<ProductDto> toDtos(List<Product> entities);

    /**
     * To entity product.
     *
     * @param dto the dto
     * @return the product
     */
    public abstract Product toEntity(ProductDto dto);

    /**
     * Populate audit.
     *
     * @param dto    the dto
     * @param entity the entity
     */
    @AfterMapping
    public void populateAudit(ProductDto dto, @MappingTarget Product entity) {
        entity.setAudit(new Audit());
    }
}

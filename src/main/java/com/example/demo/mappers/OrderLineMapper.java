package com.example.demo.mappers;

import com.example.demo.dtos.OrderLineDto;
import com.example.demo.entities.Audit;
import com.example.demo.entities.OrderLine;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

/**
 * The type Order line mapper.
 */
@Mapper(componentModel = "spring", uses = {ProductMapper.class, AuditMapper.class})
public abstract class OrderLineMapper implements DemoMapper<OrderLine, OrderLineDto> {
    /**
     * To dto order line dto.
     *
     * @param entity the entity
     * @return the order line dto
     */
    public abstract OrderLineDto toDto(OrderLine entity);

    /**
     * To dtos list.
     *
     * @param entities the entities
     * @return the list
     */
    public abstract List<OrderLineDto> toDtos(List<OrderLine> entities);

    /**
     * To entity order line.
     *
     * @param dto the dto
     * @return the order line
     */
    public abstract OrderLine toEntity(OrderLineDto dto);

    /**
     * Populate audit.
     *
     * @param dto    the dto
     * @param entity the entity
     */
    @AfterMapping
    public void populateAudit(OrderLineDto dto, @MappingTarget OrderLine entity) {
        entity.setAudit(new Audit());
    }
}

package com.example.demo.mappers;

import java.util.List;

/**
 * The interface Demo mapper.
 *
 * @param <E>   the type parameter
 * @param <DTO> the type parameter
 */
public interface DemoMapper<E, DTO> {
    /**
     * To dto dto.
     *
     * @param entity the entity
     * @return the dto
     */
    DTO toDto(E entity);

    /**
     * To dtos list.
     *
     * @param entities the entities
     * @return the list
     */
    List<DTO> toDtos(List<E> entities);

    /**
     * To entity e.
     *
     * @param dto the dto
     * @return the e
     */
    E toEntity(DTO dto);
}

package com.example.demo.mappers;

import com.example.demo.dtos.AuditDto;
import com.example.demo.entities.Audit;
import org.mapstruct.Mapper;

/**
 * The interface Audit mapper.
 */
@Mapper(componentModel = "spring")
public interface AuditMapper {
    /**
     * To dto audit dto.
     *
     * @param entity the entity
     * @return the audit dto
     */
    AuditDto toDto(Audit entity);

    /**
     * To entity audit.
     *
     * @param dto the dto
     * @return the audit
     */
    Audit toEntity(AuditDto dto);
}

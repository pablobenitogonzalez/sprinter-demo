package com.example.demo.mappers;

import com.example.demo.dtos.OrderLineBaseDto;
import com.example.demo.dtos.OrderLineDto;
import com.example.demo.entities.Audit;
import com.example.demo.entities.OrderLine;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.List;

/**
 * The type Order line base mapper.
 */
@Mapper(componentModel = "spring", uses = {ProductMapper.class, AuditMapper.class})
public abstract class OrderLineBaseMapper implements DemoMapper<OrderLine, OrderLineBaseDto> {

    /**
     * To dto order line base dto.
     *
     * @param entity the entity
     * @return the order line base dto
     */
    public abstract OrderLineBaseDto toDto(OrderLine entity);

    /**
     * To dtos list.
     *
     * @param entities the entities
     * @return the list
     */
    public abstract List<OrderLineBaseDto> toDtos(List<OrderLine> entities);

    /**
     * To entity order line.
     *
     * @param dto the dto
     * @return the order line
     */
    public abstract OrderLine toEntity(OrderLineBaseDto dto);

    /**
     * Populate audit.
     *
     * @param dto    the dto
     * @param entity the entity
     */
    @AfterMapping
    public void populateAudit(OrderLineDto dto, @MappingTarget OrderLine entity) {
        entity.setAudit(new Audit());
    }
}

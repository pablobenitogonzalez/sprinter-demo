package com.example.demo.mappers;

import com.example.demo.dtos.OrderDto;
import com.example.demo.entities.Audit;
import com.example.demo.entities.Order;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * The type Order mapper.
 */
@Mapper(componentModel = "spring", uses = {OrderLineBaseMapper.class, AuditMapper.class})
public abstract class OrderMapper implements DemoMapper<Order, OrderDto> {
    /**
     * To dto order dto.
     *
     * @param entity the entity
     * @return the order dto
     */
    public abstract OrderDto toDto(Order entity);

    /**
     * To dtos list.
     *
     * @param entities the entities
     * @return the list
     */
    public abstract List<OrderDto> toDtos(List<Order> entities);

    /**
     * To entity order.
     *
     * @param dto the dto
     * @return the order
     */
    public abstract Order toEntity(OrderDto dto);

    /**
     * Populate audit.
     *
     * @param dto    the dto
     * @param entity the entity
     */
    @AfterMapping
    public void populate(OrderDto dto, @MappingTarget Order entity) {
        entity.setAudit(new Audit());
        if (Objects.isNull(dto.getId())) {
            return;
        }
        final Order order = new Order();
        order.setId(dto.getId());
        Optional.ofNullable(entity.getOrderLines()).orElse(Collections.emptyList())
                .forEach(orderLine -> orderLine.setOrder(order));
    }
}

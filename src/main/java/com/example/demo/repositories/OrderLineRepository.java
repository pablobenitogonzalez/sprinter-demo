package com.example.demo.repositories;

import com.example.demo.entities.OrderLine;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface Order line repository.
 */
public interface OrderLineRepository extends JpaRepository<OrderLine, Long> {
}

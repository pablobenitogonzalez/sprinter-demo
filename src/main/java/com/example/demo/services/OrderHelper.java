package com.example.demo.services;

import com.example.demo.dtos.ProductDto;
import com.example.demo.entities.Order;
import com.example.demo.entities.OrderLine;
import com.example.demo.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Optional;

/**
 * The type Order helper.
 */
@Component
public class OrderHelper {

    private final ProductService productService;

    private final OrderRepository orderRepository;

    /**
     * Instantiates a new Order helper.
     *
     * @param productService  the product service
     * @param orderRepository the order repository
     */
    @Autowired
    public OrderHelper(ProductService productService, OrderRepository orderRepository) {
        this.productService = productService;
        this.orderRepository = orderRepository;
    }

    /**
     * Calculate all totals.
     *
     * @param order the order
     */
    public void calculateAllTotals(Order order) {
        order.setTotal(BigDecimal.ZERO);
        Optional.ofNullable(order.getOrderLines()).orElse(Collections.emptyList()).forEach(orderLine -> {
            calculateOrderLineTotal(orderLine);
            order.setTotal(order.getTotal().add(orderLine.getTotal()));
        });
    }

    /**
     * Calculate order line total.
     *
     * @param orderLine the order line
     */
    public void calculateOrderLineTotal(OrderLine orderLine) {
        orderLine.setTotal(BigDecimal.ZERO);
        final ProductDto productDto = productService.find(orderLine.getProduct().getId());
        orderLine.setTotal(productDto.getPrice().multiply(new BigDecimal(orderLine.getUnits())));
    }

    /**
     * Update order total.
     *
     * @param id the id
     */
    public void updateOrderTotal(Long id) {
        final Optional<Order> orderOpt = orderRepository.findById(id);
        if(orderOpt.isPresent()) {
            final Order order = orderOpt.get();
            order.setTotal(BigDecimal.ZERO);
            Optional.ofNullable(order.getOrderLines()).orElse(Collections.emptyList())
                    .forEach(orderLine -> order.setTotal(order.getTotal().add(orderLine.getTotal())));
            orderRepository.save(order);
        }
    }

}

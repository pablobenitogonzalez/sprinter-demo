package com.example.demo.services;

import com.example.demo.entities.DemoEntity;
import com.example.demo.exceptions.DemoNotFoundException;
import com.example.demo.mappers.DemoMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The type Abstract demo service.
 *
 * @param <E>   the type parameter
 * @param <DTO> the type parameter
 */
@Slf4j
public class AbstractDemoService<E extends DemoEntity, DTO> {

    /**
     * The Repository.
     */
    protected final JpaRepository<E, Long> repository;

    /**
     * The Mapper.
     */
    protected final DemoMapper<E, DTO> mapper;

    /**
     * Instantiates a new Abstract demo service.
     *
     * @param repository the repository
     * @param mapper     the mapper
     */
    public AbstractDemoService(JpaRepository<E, Long> repository, DemoMapper<E, DTO> mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    /**
     * Get e.
     *
     * @param id the id
     * @return the e
     */
    protected E get(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new DemoNotFoundException(String.format("Entity %s not found", id)));
    }

    /**
     * Find all page.
     *
     * @param pageable the pageable
     * @return the page
     */
    public Page<DTO> findAll(Pageable pageable) {
        log.debug("Finding all pageable");
        final Page<E> page = repository.findAll(pageable);
        return new PageImpl<>(mapper.toDtos(page.getContent()), page.getPageable(), page.getTotalElements());
    }

    /**
     * Find dto.
     *
     * @param id the id
     * @return the dto
     */
    public DTO find(Long id) {
        log.debug("Finding {}", id);
        return mapper.toDto(get(id));
    }

    /**
     * Delete.
     *
     * @param id the id
     */
    public void delete(Long id) {
        log.debug("Deleting {}", id);
        repository.delete(get(id));
    }
}

package com.example.demo.services;

import com.example.demo.dtos.OrderDto;
import com.example.demo.entities.Order;
import com.example.demo.mappers.OrderMapper;
import com.example.demo.repositories.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The type Order service.
 */
@Slf4j
@Service
public class OrderServiceImpl extends AbstractDemoService<Order, OrderDto> implements OrderService {

    private final OrderHelper orderHelper;

    /**
     * Instantiates a new Order service.
     *
     * @param repository  the repository
     * @param mapper      the mapper
     * @param orderHelper the order helper
     */
    @Autowired
    public OrderServiceImpl(OrderRepository repository, OrderMapper mapper, OrderHelper orderHelper) {
        super(repository, mapper);
        this.orderHelper = orderHelper;
    }

    @Override
    public OrderDto create(OrderDto orderDto) {
        log.debug("Creating new order");
        final Order order = mapper.toEntity(orderDto);
        orderHelper.calculateAllTotals(order);
        return mapper.toDto(repository.save(order));
    }

    @Override
    public OrderDto update(Long id, OrderDto orderDto) {
        log.debug("Updating order: {}", id);
        final Order orderDBO = get(id);
        orderDto.setId(id); // to prevent inconsistencies

        final Order order = mapper.toEntity(orderDto);
        orderHelper.calculateAllTotals(order);
        order.setAudit(orderDBO.getAudit());
        return mapper.toDto(repository.save(order));
    }
}

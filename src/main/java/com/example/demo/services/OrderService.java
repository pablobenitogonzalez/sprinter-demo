package com.example.demo.services;

import com.example.demo.dtos.OrderDto;

/**
 * The interface Order service.
 */
public interface OrderService extends DemoService<OrderDto> {
}

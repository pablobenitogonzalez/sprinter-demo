package com.example.demo.services;

import com.example.demo.dtos.ProductDto;

/**
 * The interface Product service.
 */
public interface ProductService extends DemoService<ProductDto> {
}

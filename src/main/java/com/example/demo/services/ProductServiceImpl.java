package com.example.demo.services;

import com.example.demo.dtos.ProductDto;
import com.example.demo.entities.Product;
import com.example.demo.mappers.ProductMapper;
import com.example.demo.repositories.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * The type Product service.
 */
@Slf4j
@Service
public class ProductServiceImpl extends AbstractDemoService<Product, ProductDto> implements ProductService {

    /**
     * Instantiates a new Product service.
     *
     * @param repository the repository
     * @param mapper     the mapper
     */
    @Autowired
    public ProductServiceImpl(ProductRepository repository, ProductMapper mapper) {
        super(repository, mapper);
    }

    /**
     * Ehcache basic configuration
     */
    @Override
    @Cacheable(cacheNames = "product", key="#id")
    public ProductDto find(Long id) {
        log.info("Returning product {}", id);
        return super.find(id);
    }

    @Override
    public ProductDto create(ProductDto productDto) {
        log.debug("Creating new product");
        return mapper.toDto(repository.save(mapper.toEntity(productDto)));
    }

    @Override
    public ProductDto update(Long id, ProductDto productDto) {
        log.debug("Updating product: {}", id);
        final Product productDBO = get(id);
        productDBO.setId(id); // to prevent inconsistencies

        final Product product = mapper.toEntity(productDto);
        product.setAudit(productDBO.getAudit());
        return mapper.toDto(repository.save(product));
    }
}

package com.example.demo.services;

import com.example.demo.dtos.OrderLineDto;

/**
 * The interface Order line service.
 */
public interface OrderLineService extends DemoService<OrderLineDto> {
}

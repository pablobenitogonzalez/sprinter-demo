package com.example.demo.services;

import com.example.demo.dtos.OrderLineDto;
import com.example.demo.entities.OrderLine;
import com.example.demo.mappers.OrderLineMapper;
import com.example.demo.repositories.OrderLineRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * The type Order line service.
 */
@Slf4j
@Service
public class OrderLineServiceImpl extends AbstractDemoService<OrderLine, OrderLineDto> implements OrderLineService {

    private final OrderHelper orderHelper;

    /**
     * Instantiates a new Order line service.
     *
     * @param repository  the repository
     * @param mapper      the mapper
     * @param orderHelper the order helper
     */
    @Autowired
    public OrderLineServiceImpl(OrderLineRepository repository, OrderLineMapper mapper, OrderHelper orderHelper) {
        super(repository, mapper);
        this.orderHelper = orderHelper;
    }

    @Override
    @Transactional
    public OrderLineDto create(OrderLineDto orderLineDto) {
        log.debug("Creating new order line");
        final OrderLine orderLine = mapper.toEntity(orderLineDto);
        orderHelper.calculateOrderLineTotal(orderLine);

        final OrderLine savedOrderLine = repository.saveAndFlush(orderLine);
        orderHelper.updateOrderTotal(orderLine.getOrder().getId()); // update order
        return mapper.toDto(savedOrderLine);
    }

    @Override
    @Transactional
    public OrderLineDto update(Long id, OrderLineDto orderLineDto) {
        log.debug("Updating order line: {}", id);
        final OrderLine orderLineDBO = get(id);
        orderLineDto.setId(id); // to prevent inconsistencies
        final Long oldOrderId = orderLineDBO.getOrder().getId();

        final OrderLine orderLine = mapper.toEntity(orderLineDto);
        orderHelper.calculateOrderLineTotal(orderLine);
        orderLine.setAudit(orderLineDBO.getAudit());
        final OrderLine savedOrderLine = repository.saveAndFlush(orderLine);

        if (!oldOrderId.equals(orderLineDto.getOrder().getId())) {
            orderHelper.updateOrderTotal(oldOrderId); // update old order
        }
        orderHelper.updateOrderTotal(orderLineDto.getOrder().getId()); // update new order
        return mapper.toDto(savedOrderLine);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        final OrderLine orderLine = get(id);
        final Long orderId = orderLine.getOrder().getId();
        repository.delete(orderLine);
        repository.flush();
        orderHelper.updateOrderTotal(orderId);
    }
}

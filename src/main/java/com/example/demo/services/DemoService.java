package com.example.demo.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * The interface Demo service.
 *
 * @param <DTO> the type parameter
 */
public interface DemoService<DTO> {

    /**
     * Find all page.
     *
     * @param pageable the pageable
     * @return the page
     */
    Page<DTO> findAll(Pageable pageable);

    /**
     * Find dto.
     *
     * @param id the id
     * @return the dto
     */
    DTO find(Long id);

    /**
     * Create dto.
     *
     * @param orderDto the order dto
     * @return the dto
     */
    DTO create(DTO orderDto);

    /**
     * Update dto.
     *
     * @param id       the id
     * @param orderDto the order dto
     * @return the dto
     */
    DTO update(Long id, DTO orderDto);

    /**
     * Delete.
     *
     * @param id the id
     */
    void delete(Long id);
}

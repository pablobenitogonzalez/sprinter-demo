package com.example.demo.controllers;

import com.example.demo.dtos.OrderLineDto;
import com.example.demo.services.OrderLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The type Order line controller.
 */
@RestController
@RequestMapping("/order-line")
public class OrderLineController extends AbstractDemoController<OrderLineDto> {

    /**
     * Instantiates a new Order line controller.
     *
     * @param service the service
     */
    @Autowired
    public OrderLineController(OrderLineService service) {
        super(service);
    }

}

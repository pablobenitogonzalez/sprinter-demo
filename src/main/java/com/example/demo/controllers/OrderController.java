package com.example.demo.controllers;

import com.example.demo.dtos.OrderDto;
import com.example.demo.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The type Order controller.
 */
@RestController
@RequestMapping("/order")
public class OrderController extends AbstractDemoController<OrderDto> {

    /**
     * Instantiates a new Order controller.
     *
     * @param service the service
     */
    @Autowired
    public OrderController(OrderService service) {
        super(service);
    }

}

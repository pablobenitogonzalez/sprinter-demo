package com.example.demo.controllers;

import com.example.demo.services.DemoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * The type Abstract demo controller.
 *
 * @param <DTO> the type parameter
 */
@Slf4j
public class AbstractDemoController<DTO> {

    protected final DemoService<DTO> service;

    /**
     * Instantiates a new Abstract demo controller.
     *
     * @param service the service
     */
    public AbstractDemoController(DemoService<DTO> service) {
        this.service = service;
    }

    /**
     * Find all page.
     *
     * @param pageable the pageable
     * @return the page
     */
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public Page<DTO> findAll(final Pageable pageable) {
        return service.findAll(pageable);
    }

    /**
     * Find dto.
     *
     * @param id the id
     * @return the dto
     */
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public DTO find(final @PathVariable Long id) {
        return service.find(id);
    }

    /**
     * Create dto.
     *
     * @param dto the dto
     * @return the dto
     */
    @PostMapping()
    @ResponseStatus(HttpStatus.OK)
    public DTO create(final @RequestBody @Valid DTO dto) {
        return service.create(dto);
    }

    /**
     * Update dto.
     *
     * @param dto the dto
     * @return the dto
     */
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public DTO update(final @PathVariable Long id, final @RequestBody @Valid DTO dto) {
        return service.update(id, dto);
    }

    /**
     * Delete.
     *
     * @param id the id
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(final @PathVariable Long id) {
        service.delete(id);
    }
}

package com.example.demo.controllers;

import com.example.demo.dtos.ProductDto;
import com.example.demo.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The type Product controller.
 */
@RestController
@RequestMapping("/product")
public class ProductController extends AbstractDemoController<ProductDto> {

    /**
     * Instantiates a new Product controller.
     *
     * @param service the service
     */
    @Autowired
    public ProductController(ProductService service) {
        super(service);
    }

}

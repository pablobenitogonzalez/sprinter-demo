insert into product (id, name, description, price, created_on, updated_on) values (1, 'Merrell Annex', 'Zapato Montaña Hombre', 87.99, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());
insert into product (id, name, description, price, created_on, updated_on) values (2, 'Nike Md Valiant', 'Zapatillas Chica', 39.99, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());
insert into product (id, name, description, price, created_on, updated_on) values (3, 'Adidas Daily 3.0', 'Zapatillas Hombre', 43.99, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());
insert into product (id, name, description, price, created_on, updated_on) values (4, 'Fila Forerunner', 'Zapatillas Hombre', 31.99, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());
insert into product (id, name, description, price, created_on, updated_on) values (5, 'Puma Turino', 'Zapatillas Chica', 47.99, CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

